// Import the react library
import React, {Component} from "react"
import ReactDOM from "react-dom"
import axios from "axios"
import ImageList from "./components/ImageList"

// Create a component
class App extends Component {

    // whenever we create/call object from this class constructor method gonna be called
    constructor(props) {
        super(props);
        console.log("Props: " + props)

        // initializing state object
        this.state = {images: []}
    }

    componentWillMount() {
        // This happened before startup or full load page
        // Good place to load data
        console.log("App is about to render")
        axios.get("https://api.imgur.com/3/gallery/hot/top/0", {
        headers: {
            Authorization: "Client-ID 7479a679d09b0f5"
        }
        }).then(response => this.setState({images: response.data.data}))
    }
    render() {
        return (
            <div>
                <ImageList images={this.state.images}/>
            </div>
        )
    }
}

// const App = () => {
//     return (
//         <div>
//             <ImageList/>
//         </div>
//     )
// }

// Render this component to the screen

Meteor.startup(() => {
    ReactDOM.render(<App />, document.getElementById("root"))
});