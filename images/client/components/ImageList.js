//Create ouy image list component
import React from "react"
import ImageDetail from "./ImageDetail"

const ImageList = (props) => {

    const validImages = props.images.filter(image => !image.is_album)

    // summarty comments about arrays
    const RenderImages = props.images
        .filter(image => !image.is_album)
        .map((image) => {
            return <ImageDetail key={image.title} image={image}/>
    })

    return (
        <ul className="media-list list-group">
            {RenderImages}
        </ul>
    )
}

export default ImageList