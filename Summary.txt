Installation:
    - npm install -- save react = instaluje reacta z zależnościami które są nam potrzebne
    - npm install --save react-dom
    - npm install --save faker lodash (faker do pupoluacji fakewoymi danymi) (lodash do zapełniania obiektu danymi ala pętla)
    - [BOOTSTRAP] neteor add twbs:bootstrap@3.3.6
    - meteor remove autopublish (usuwa dependency dzięki czemu wszystkie dane nie będą prezentowane z automatu)
    - npm install --save react-addons-pure-render-mixin 
    - meteor add react-meteor-data (allow create container)
Usunięte z package.json
    - "meteor": {
        "mainModule": {
        "client": "client/main.js",
        "server": "server/main.js"
        },
    "testModule": "tests/main.js"
  }
Tablice z danymi:
    - robimy tablicę z danymi;
    - robimy funkcję like const zmiennaConstZFunkcją = nazwaTablicy.map((image) => { return (<jakiśDiv albo component />) } )
    - przekazujemy do componentu wartośći z tablicy poprzez parametr, czyli np. image={image} (pierwsze to nazwa parametry, drugie przekazanie obiektu)
        - teraz w pliku z componentem w funkcji która zwraca html w parametrze przekazujemy props, czyli cont zmiennaDwa = (props) => { return <div></div> }
        - dzięki temu w zależności od wielkości obiektu w html możemy przekazać wszystkie wartości, czyli <div>{props.image.title}</div>, <div>{props.image.price}</div>
    - zwracamy w głównym html nazwę const (zamiast nazwy <JakiśComponent/> zwracamy {zmiennaConstZFunkcją})
IMGUR:
    client secret: 5c2a8ac4c8ca8cd18054665bc3d7c073f0c60112
Constructor:
    - tworzymy konstruktor by odświeżyć i zapełnić danymi obiekt kiedy strona się już załaduje
    - tworzymy w nim this.state = { images: []} pustą tablicę nazwaną jakkolwiek
    - aby zrobić z niej użytek, korzystająć z biblioteki axiox w metodzie .then parametr zwracamy w postaci this.setState({images: response.data.data}) w czym ostatnie to to co 
    chcemy tam wrzucić z response
Troubleshooting:
    - avatary nie pokazywały się w projekcie na stronie (w konsoli meteor reset, a później połączenie się ze stroną - zadziałało)