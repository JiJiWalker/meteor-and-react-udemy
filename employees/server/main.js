// Only executed on the server
import _ from "lodash"
import {Meteor} from "meteor/meteor"
import {Employees} from "../imports/collections/Employees"
import {image, helpers} from "faker"

Meteor.startup(() => {
    // Generate data

    // Check if data exist in collection
    // See if the collection has any record
    const numberRecord = Employees.find({}).count();
    console.log(numberRecord);

    if (!numberRecord) {
        // generate some data...
        // function generate 5k times
        _.times(5000, () => {
            // same as const name = helper.creatCrad().name
            const {name, email, phone} = helpers.createCard();

            // Zapełniamy obiekt DB
            Employees.insert({
                // w przypadku kiedy key:value jest nazwane tak samo można napisać samo key:
                name, email, phone,
                // name: name,
                // email: email,
                // phone: phone,
                avatar: image.avatar()
            })
        })
    }

    Meteor.publish("employees", function() {
        // drugi parametr to limit ile ma zwrócić
        return Employees.find({}, {limit: 20});
    })
})