import React from "react"
import {createContainer} from "meteor/react-meteor-data"
import { Meteor } from "meteor/meteor";
import {Employees} from "../../imports/collections/Employees"
import EmployeeDetails from "./EmployeeDetails"


const EmployeeList = (props) => {
    // property z subscibe ala employees będzie dostępne zaraz za props
    // czyli props employees = array of employee object
    // console.log(props.employees)

    const fill = props.employees.map(function(employee) {
        return (
            <EmployeeDetails key={employee._id} employee={employee}/>
        )
    })

    return (
        <div>
            <div className="employee-list">
                {/* {props.employees.map(employee => <EmployeeDetails/>)} */}
                {fill}
            </div>
        </div>
    )

}

export default createContainer(() => {
    // set up subscribsion
    Meteor.subscribe("employees");
    // return an object. Whatever we return will be sent to EmployeeList as props
    return {employees: Employees.find({}).fetch()}

}, EmployeeList)