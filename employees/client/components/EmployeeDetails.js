import React from "react"

const EmployeeDetails = ({employee}) => {
    // props.employee is our employee model
    // props.employee to samo co {employee}

    // decompresion
    const {name, email, phone, avatar} = employee;

    return (
        <div className="thumbnail">
            <img src={avatar}/>
            {console.log(avatar)}
            <div className="caption">
                <h3>{name}</h3>
                <ul className="list-group">
                    <li className="list-group-item">Email: {email}</li>
                    <li className="list-group-item">Phone: {phone}</li>
                </ul>
            </div>
        </div>
    )
}

export default EmployeeDetails